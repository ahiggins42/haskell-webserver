module View exposing (renderRoute, view)

import Browser
import Element exposing (..)
import Element.Events exposing (..)
import Element.Input exposing (button)
import Element.Region exposing (..)
import Router.Routes exposing (..)
import Router.Types exposing (Msg(..))
import Styles
import Types exposing (..)
import Users.View


view : Model -> Browser.Document Types.Msg
view model =
    { title = "client"
    , body = [ Element.layout [] <| el [ width (px 800), centerX ] (renderRoute model) ]
    }


renderRoute : Model -> Element Types.Msg
renderRoute model =
    case model.router.page of
        Home ->
            column [ spacing 5 ]
                [ el ([ heading 1 ] ++ Styles.title) (text "Welcome")
                , row [ spacing 5 ]
                    []
                ]

        NotFound ->
            text "404 Not Found"

        UsersPage ->
            Element.map MsgForUsers (Users.View.view model.users)
