module Types exposing (Model, Msg(..))

import Router.Types
import Users.Types


type alias Model =
    { router : Router.Types.Model, users : Users.Types.Model }


type Msg
    = MsgForRouter Router.Types.Msg
    | MsgForUsers Users.Types.Msg
