module Users.Types exposing (Model, Msg(..))

import Data.User exposing (..)
import Http exposing (..)


type alias Model =
    { users : List User
    }


type Msg
    = Load
    | Loaded (Result Http.Error (List User))
