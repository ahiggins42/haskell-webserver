module Users.Update exposing (init, update, updateUsers)

import Http
import Request.Default exposing (usersGet)
import Return exposing (Return, return)
import Task exposing (perform, succeed)
import Types
import Users.Types exposing (Model, Msg(..))


init : Return Msg Model
init =
    return
        { users = []
        }
        (perform
            (\_ -> Load)
            (succeed Nothing)
        )


update : Types.Msg -> Model -> Return Msg Model
update msgFor model =
    case msgFor of
        Types.MsgForUsers msg ->
            updateUsers msg model

        _ ->
            return model Cmd.none


updateUsers : Msg -> Model -> Return Msg Model
updateUsers msg model =
    case msg of
        Load ->
            return model (Http.send Loaded <| usersGet)

        Loaded (Ok users) ->
            return { model | users = users } Cmd.none

        Loaded (Err _) ->
            return model Cmd.none
