module Users.View exposing (view)

import Element exposing (..)
import Styles exposing (..)
import Users.Types exposing (..)


view : Model -> Element Msg
view model =
    table []
        { data = model.users
        , columns =
            [ { header = text "Name"
              , width = fill
              , view = \user -> text user.name
              }
            ]
        }
