{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module Users.Models where

import GHC.Generics(Generic)
import Data.Typeable

data User = User
  { name :: String
  , age :: Int
  , email :: String
  } deriving (Eq, Show, Generic, Typeable)
    