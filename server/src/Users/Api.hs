{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
module Users.Api where

import Control.Lens
import Data.Aeson
import Data.Swagger
import Servant
import Servant.Swagger

import Users.Models
import Users.Mongo

type UserAPI = "users" :> (    Get '[JSON] [User] 
                          :<|> ReqBody '[JSON] User :> Post '[JSON] String
                          )

instance ToJSON User
instance FromJSON User

instance ToSchema User where
    declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
      & mapped.schema.description ?~ "This is some real User right here"
      & mapped.schema.example ?~ toJSON isaac

isaac :: User
isaac = User "Isacc Newton" 372 "isaac@newton.co.uk"

albert :: User
albert = User "Albert Einstein" 136 "ae@mc2.org"       

users1 :: [User]
users1 = [ isaac
         , albert
         ]

userApi :: Server UserAPI
userApi = getUsers :<|> addUser