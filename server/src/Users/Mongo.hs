{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
module Users.Mongo where

import Control.Monad.IO.Class
import Data.Bson.Mapping
import Database.MongoDB

import Users.Models

$(deriveBson ''User)

getUserDocuments :: IO [Document]
getUserDocuments = doMongo $ rest =<< Database.MongoDB.find (select [] "users")

getUserObjects :: IO [Document] -> IO [User]
getUserObjects = (=<<) $ mapM fromBson

doMongo :: MonadIO m => Action m a -> m a
doMongo action = do
  pipe <- liftIO $ connect (Database.MongoDB.host "database")
  result <- access pipe master "test"  action
  liftIO $ close pipe
  return result

getUsers :: MonadIO m => m [User]
getUsers =
  liftIO $ getUserObjects getUserDocuments

addUser :: MonadIO m => User -> m String
addUser x = do
  val <- doMongo $ insert "users" $ toBson x 
  case val of
    ObjId (oid) -> return $ show oid
    _           -> fail $ "unexpected id"