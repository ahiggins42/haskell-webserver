{-# LANGUAGE OverloadedStrings #-}
module ApacheLogs.Parse where

import Control.Applicative
import Data.Attoparsec.ByteString.Char8
import Data.Word
import Data.Maybe
import Data.String.Conversions

import ApacheLogs.Models

parseApacheLog :: Parser ApacheLog
parseApacheLog = many $ parseApacheLogEntry <* endOfLine

parseApacheLogEntry :: Parser ApacheLogEntry
parseApacheLogEntry = do
    remoteHost <- parseRemoteHost
    space
    identD <- parseMaybeString
    space
    user <- parseMaybeString
    return $ ApacheLogEntry remoteHost identD user

parseMaybeString :: Parser (Maybe String)
parseMaybeString = parseUnavailableData <|> parseMaybeStringValue

parseMaybeStringValue :: Parser (Maybe String)
parseMaybeStringValue = do
    value <- takeTill (\x -> x == ' ' || x == '\n')
    return $ Just $ cs value

parseRemoteHost :: Parser (Maybe RemoteHost)
parseRemoteHost = parseUnavailableData <|> parseIPAddress <|> parseRemoteHostName

parseUnavailableData :: Parser (Maybe a)
parseUnavailableData = do
    char '-'
    return Nothing

parseIPAddress :: Parser (Maybe RemoteHost)
parseIPAddress = do
    d1 <- decimal
    char '.'
    d2 <- decimal
    char '.'
    d3 <- decimal
    char '.'
    d4 <- decimal
    return $ Just $ RemoteHostIPAddress $ IPAddress d1 d2 d3 d4

parseRemoteHostName :: Parser (Maybe RemoteHost)
parseRemoteHostName = do
    hostName <- takeTill (\x -> x == ' ' || x == '\n')
    return $ Just $ RemoteHostHostName $ cs hostName