{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
module ApacheLogs.Api where

import Data.Aeson
import Data.Attoparsec.ByteString
import Data.Maybe
import Data.String.Conversions
import Data.Swagger
import Data.Typeable
import GHC.Generics(Generic)
import Servant
import Servant.Swagger

import ApacheLogs.Models as ALM
import ApacheLogs.Parse

type LogAPI = "logs" :> ReqBody '[JSON] ApacheLogPost :> Post '[JSON] ApacheLog

instance ToJSON ApacheLogPost
instance FromJSON ApacheLogPost
instance ToJSON IPAddress
instance FromJSON IPAddress
instance ToJSON ALM.RemoteHost
instance FromJSON ALM.RemoteHost
instance ToJSON ApacheLogEntry
instance FromJSON ApacheLogEntry

instance ToSchema ApacheLogPost where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy

instance ToSchema IPAddress where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy

instance ToSchema ALM.RemoteHost where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy

instance ToSchema ApacheLogEntry where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy

data ApacheLogPost = ApacheLogPost {
  logString :: String
} deriving (Eq, Show, Generic, Typeable)

convertLogs :: ApacheLogPost -> Handler ApacheLog
convertLogs x =
    let
      result = parseOnly parseApacheLog $ cs (logString x)
    in
      case result of
        Right logs -> return logs
        Left err -> throwError $ ServantErr 400 err "" []

logApi :: Server LogAPI
logApi = convertLogs