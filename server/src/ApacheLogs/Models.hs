{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module ApacheLogs.Models where

import GHC.Generics(Generic)
import Data.Typeable
import Data.Word
import Data.Maybe

data IPAddress = IPAddress Word8 Word8 Word8 Word8 deriving (Eq, Show, Generic, Typeable)

type RemoteHostName = String

data RemoteHost = RemoteHostIPAddress IPAddress | RemoteHostHostName RemoteHostName
    deriving (Eq, Show, Generic, Typeable)

data ApacheLogEntry = ApacheLogEntry
    { remoteHost :: Maybe RemoteHost
    , identD :: Maybe String
    , user :: Maybe String
    } deriving (Eq, Show, Generic, Typeable)

type ApacheLog = [ApacheLogEntry]