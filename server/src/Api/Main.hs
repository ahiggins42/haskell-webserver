{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
module Api.Main(server) where

import Control.Lens
import Data.Swagger
import Data.Typeable
import Servant 
import Servant.Swagger
import Network.Wai
import Network.Wai.Handler.Warp(runSettings, defaultSettings, setLogger, setPort)
import Network.Wai.Logger(withStdoutLogger)
import Network.Wai.Middleware.Cors

import ApacheLogs.Api
import Users.Api
import Sudoku.Api

dataAPI :: Proxy DataAPI
dataAPI = Proxy

api :: Proxy API
api = Proxy

type SwaggerAPI = "swagger.json" :> Get '[JSON] Swagger

type DataAPI = UserAPI :<|> LogAPI :<|> SudokuAPI

type API = DataAPI :<|> SwaggerAPI

swagger :: Swagger
swagger = toSwagger dataAPI
  & info.title     .~ "User API"
  & info.version   .~ "1.0"
  & info.description ?~ "This is an API that tests swagger generation"
  & info.license ?~ ("MIT" & url ?~ URL "http://mit.com")

server1 :: Server API
server1 = (userApi :<|> logApi :<|> sudokuApi) :<|> return swagger

app1 :: Application
app1 = serve api server1

apiCors :: Middleware
apiCors = cors $ const (Just apiResourcePolicy)

apiResourcePolicy :: CorsResourcePolicy
apiResourcePolicy =
  CorsResourcePolicy
    { corsOrigins = Nothing -- gives you /*
    , corsMethods = ["GET", "POST", "PUT", "DELETE", "HEAD", "OPTION"]
    , corsRequestHeaders = simpleHeaders -- adds "Content-Type" to defaults
    , corsExposedHeaders = Nothing
    , corsMaxAge = Nothing
    , corsVaryOrigin = False
    , corsRequireOrigin = False
    , corsIgnoreFailures = False
    }

server :: IO ()
server = do
  withStdoutLogger $ \aplogger -> do
      let settings = setPort 8080 $ setLogger aplogger defaultSettings
      runSettings settings $ apiCors $ app1
