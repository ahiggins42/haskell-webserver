{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
module Sudoku.Api where

import Data.Aeson
import Data.Maybe
import Data.Swagger
import Servant
import Servant.Swagger

import Sudoku.Models
import Sudoku.Solver

instance ToJSON Cell
instance FromJSON Cell

type SudokuAPI = "sudoku" :> ReqBody '[JSON] String :> Post '[JSON] (Maybe Grid)

instance ToSchema Cell where
    declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy

handleSudoku :: String -> Handler (Maybe Grid)
handleSudoku = return . solveSudoku . fromJust . readGrid

sudokuApi :: Server SudokuAPI
sudokuApi = handleSudoku