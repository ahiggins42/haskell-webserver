{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module Sudoku.Models where

import GHC.Generics(Generic)
import Data.Typeable

data Cell = Fixed Int | Possible [Int] deriving (Eq, Show, Generic, Typeable)
type Row = [Cell]
type Grid = [Row]